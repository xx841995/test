package com.activitytype.model;

import java.util.List;
import java.util.stream.Collectors;

import com.activity.model.ActivityService;
import com.activity.model.ActivityVO;

public class Test {

	public static void main(String[] args) {
		
		
	
		//將所有活動抓出
		ActivityService activitySvc = new ActivityService();
		List<ActivityVO> list = activitySvc.getAll();
		//濾出審核中活動		
		List<ActivityVO> listA0 = list.stream().filter(e -> e.getStatus().equals("a0"))
				.collect(Collectors.toList());
		//濾出通過活動
		List<ActivityVO> listA1 = list.stream().filter(e -> e.getStatus().equals("a1"))
				.collect(Collectors.toList());
		//濾出未通過活動
		List<ActivityVO> listA2 = list.stream().filter(e -> e.getStatus().equals("a2"))
				.collect(Collectors.toList());
		//濾出編輯中活動
		List<ActivityVO> listA3 = list.stream().filter(e -> e.getStatus().equals("a3"))
				.collect(Collectors.toList());
		
		
		System.out.println(list);
		System.out.println(listA0);
		System.out.println(listA1);
	}

}
