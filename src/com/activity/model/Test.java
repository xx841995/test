package com.activity.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Test {

	public static void main(String[] args) {
		
		
		//將所有活動抓出
		ActivityDAO dao = new ActivityDAO();
		List<ActivityVO> list =  dao.getAll();			
		//濾出審核中活動		
		List<ActivityVO> listA0 = list.stream()
						.filter(e -> e.getStatus().equals("a0"))
						.collect(Collectors.toList());
		//濾出通過活動
		List<ActivityVO> listA1 = list.stream()
				.filter(e -> e.getStatus().equals("a1"))
				.collect(Collectors.toList());
		//濾出未通過活動
		List<ActivityVO> listA2 = list.stream()
				.filter(e -> e.getStatus().equals("a2"))
				.collect(Collectors.toList());
		//濾出編輯中活動
		List<ActivityVO> listA3 = list.stream()
				.filter(e -> e.getStatus().equals("a3"))
				.collect(Collectors.toList());
		
		
		
		System.out.println(list);
	
		
	}

}
