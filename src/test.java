import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import com.manager_msg.model.Manager_msgJDBCDAO;
import com.manager_msg.model.Manager_msgVO;

public class test {

	public static void main(String[] args) {
		
		
		Manager_msgJDBCDAO dao = new Manager_msgJDBCDAO();
		
		List<Manager_msgVO> list = dao.getAll();
		System.out.println();
		
		for(Manager_msgVO msgVO : list) {
			DateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");		
			System.out.println(sdf.format(msgVO.getEstablished_time()));
		}
		
		
		
	}

}
