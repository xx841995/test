<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
     <link href="https://fonts.googleapis.com/css2?family=Kosugi+Maru&display=swap" rel="stylesheet" />
    <link rel="stylesheet" href="./css/basic.css" />
    <link rel="stylesheet" href="./css/baclemd,amagercheack1.css">
    <title>活動管理</title>
  </head>
  <body>
    <div class="main">
      <div class="main-div">
        <div class="panel-div">
          <!-- 姓名圖片 -->
          <div class="name">
            <img src="./img/tony.jfif" alt="" />
            <p>金城武</p>
          </div>
          <!-- 功能版面 -->

          <!-- 首頁公告 -->
          <div class="homepage">
            <a href=""><img src="./img/house.svg" alt=""><p>首頁公告</p></a>
          </div>

          <!-- 人員管理 -->
          <div class="worker_management">
            <a href=""><img src="./img/member.svg" alt=""><p>人員管理</p></a>
          </div>
          <!-- 管理員列表 -->
          <div class="worker_management_list none">
            <a href=""><p>管理員列表</p></a>
          </div>
        
        <!-- 檢舉列表 -->
          <div class="report_list">
            <a href=""><img src="./img/list.svg" alt=""><p>檢舉列表</p></a>
          </div>
         <!-- 檢舉清單  待審核名單 -->
         <div class="report_list_all none">
            <a href=""><p>檢舉清單</p></a>
            <a href=""><p>待審核名單</p></a>
          </div>
            <!-- 活動管理 -->
            <div class="event_management">
                <a href=""><img src="./img/events.svg" alt=""><p>活動管理</p></a>
            </div>
    
            <!-- 會員管理 -->
            <div class="member_management">
                <a href=""><img src="./img/people.svg" alt=""><p>會員管理</p></a>
            </div>
        </div>
  
        <!-- 內容板塊 -->
        <div class="panel-text">
          <header class="header">
            <div class="h1">
              <p></p>
            </div>
            <div>
              <a href=""><img class="logo" src="img\APE3.svg" alt="" /></a>
            </div>
          </header>
          <div class="line"></div>

          <div class="body_content_data">
            <div class="container">
              <h2>活動管理</h2>
              <select>
                <option>待審核</option>
                <option>通過</option>
                <option>未通過</option>
                <option>編輯中</option>
              </select>


              <div class="over">
              <ul class="responsive-table">
                <li class="table-header">
                  <div class="header-1">活動名稱</div>
                  <div class="header-2">活動日期</div>
                  <div class="header-3">活動類型</div>
                  <div class="header-4">活動簡介</div>
                  <div class="header-5">活動示意</div>
                  <div class="header-6">審核</div>
                </li>
                <li class="table-row">
                  <div class="col col-1" data-label="ACTIVITY_NAME">溜溜貓友善市集</div>
                  <div class="col col-2" data-label="ACTIVITY_DATE">2021-10-20 2021-10-22</div>
                  <div class="col col-3" data-label="ACT_TYPE_NAME">寵物用品</div>
                  <div class="col col-4" data-label="INTRODUCTION">
                    將文創攤位的高度將到毛小孩的視角，讓毛小孩也能跟主人一起參觀，打造人與寵物間和諧共處的友善環境，顛覆過往以人為需求的概念，改以毛小孩需求為主的場域空間。</div>
                  <div class="col col-5" data-label="ACTIVITY_PIC"><img src="img_act/狗狗1.jpg" alt=""></div>
                  <div class="col col-6" data-label="STATUS"><span class="STATUS">待審核</span></div>
                </li>
                <li class="table-row">
                  <div class="col col-1" data-label="ACTIVITY_NAME">溜溜貓友善市集</div>
                  <div class="col col-2" data-label="ACTIVITY_DATE">2021-10-20 2021-10-22</div>
                  <div class="col col-3" data-label="ACT_TYPE_NAME">寵物用品</div>
                  <div class="col col-4" data-label="INTRODUCTION">
                    將文創攤位的高度將到毛小孩的視角，讓毛小孩也能跟主人一起參觀，打造人與寵物間和諧共處的友善環境，顛覆過往以人為需求的概念，改以毛小孩需求為主的場域空間。</div>
                  <div class="col col-5" data-label="ACTIVITY_PIC"><img src="img_act/狗狗1.jpg" alt=""></div>
                  <div class="col col-6" data-label="STATUS"><span class="STATUS">待審核</span></div>
                </li>
                <li class="table-row">
                  <div class="col col-1" data-label="ACTIVITY_NAME">溜溜貓友善市集</div>
                  <div class="col col-2" data-label="ACTIVITY_DATE">2021-10-20 2021-10-22</div>
                  <div class="col col-3" data-label="ACT_TYPE_NAME">寵物用品</div>
                  <div class="col col-4" data-label="INTRODUCTION">
                    將文創攤位的高度將到毛小孩的視角，讓毛小孩也能跟主人一起參觀，打造人與寵物間和諧共處的友善環境，顛覆過往以人為需求的概念，改以毛小孩需求為主的場域空間。</div>
                  <div class="col col-5" data-label="ACTIVITY_PIC"><img src="img_act/狗狗1.jpg" alt=""></div>
                  <div class="col col-6" data-label="STATUS"><span class="STATUS">待審核</span></div>
                </li>
                <li class="table-row">
                  <div class="col col-1" data-label="ACTIVITY_NAME">溜溜貓友善市集</div>
                  <div class="col col-2" data-label="ACTIVITY_DATE">2021-10-20 2021-10-22</div>
                  <div class="col col-3" data-label="ACT_TYPE_NAME">寵物用品</div>
                  <div class="col col-4" data-label="INTRODUCTION">
                    將文創攤位的高度將到毛小孩的視角，讓毛小孩也能跟主人一起參觀，打造人與寵物間和諧共處的友善環境，顛覆過往以人為需求的概念，改以毛小孩需求為主的場域空間。</div>
                  <div class="col col-5" data-label="ACTIVITY_PIC"><img src="img_act/狗狗1.jpg" alt=""></div>
                  <div class="col col-6" data-label="STATUS"><span class="STATUS">待審核</span></div>
                </li>
                
              </ul>
            </div>
         
       
              <!-- 每一筆活動列出來 -->
      
            </div>
       
        </div>
      </div>
    </div>
  </body>
</html>
