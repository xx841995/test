<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/backstage/css/basic.css" />
<link href="https://fonts.googleapis.com/css2?family=Kosugi+Maru&display=swap" rel="stylesheet"/>
<title>基本版面</title>
</head>
<body>
	<div class="block1"></div>
	<div class="bg"><img src="<%=request.getContextPath()%>/backstage/img\bg.png" alt=""></div>
	<div class="main">
		<div class="main-div">
			<div class="panel-div">
				<!-- 姓名圖片 -->
				<div class="name">
					<img src="./img/tony.jfif" alt="" />
					<p>金城武</p>
				</div>

				<!-- 功能版面 -->

				<!-- 首頁公告 -->
				<div class="homepage">
					<a href="">
						<img src="./img/house.svg" alt="">
						<p>首頁公告</p>
					</a>
				</div>

				<!-- 人員管理 -->
				<div class="worker_management">
					<a href="">
						<img src="./img/member.svg" alt="">
						<p>人員管理</p>
					</a>
				</div>
				<!-- 管理員列表 -->
				<div class="worker_management_list none">
					<a href="">
						<p>管理員列表</p>
					</a>
				</div>

				<!-- 檢舉列表 -->
				<div class="report_list">
					<a href="">
						<img src="./img/list.svg" alt="">
						<p>檢舉列表</p>
					</a>
				</div>
				<!-- 檢舉清單  待審核名單 -->
				<div class="report_list_all none">
					<a href="">
						<p>檢舉清單</p>
					</a>
					<a href="">
						<p>待審核名單</p>
					</a>
				</div>
				<!-- 活動管理 -->
				<div class="event_management">
					<a href="">
						<img src="./img/events.svg" alt="">
						<p>活動管理</p>
					</a>
				</div>

				<!-- 會員管理 -->
				<div class="member_management">
					<a href="">
						<img src="./img/people.svg" alt="">
						<p>會員管理</p>
					</a>
				</div>
			</div>

			<!-- 內容板塊 -->
			<div class="panel-text">




				<jsp:include page="chat1.jsp">
					<jsp:param name="a" value="1" />
					<jsp:param name="b" value="2" />
				</jsp:include>





			</div>
		</div>
	</div>
	<div class="block"></div>
</body>
</html>
